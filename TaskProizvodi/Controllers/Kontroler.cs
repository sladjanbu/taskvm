﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskProizvodi.Controllers
{
    public class Kontroler
    {
        protected TaskWM_DBEntities ent;

        public Kontroler()
        {
            ent = new TaskWM_DBEntities();
        }

        public List<Proizvodi> vratiProizvode()
        {
            List<Proizvodi> proizvodi = ent.Proizvodis.Where(x => x.Id != -1).ToList();
            return proizvodi;
        }
        public Proizvodi vratiProizvod(int id)
        {
            Proizvodi proizvod = ent.Proizvodis.Where(x => x.Id == id).FirstOrDefault();
            return proizvod;
        }
        public void izmenaProizvoda(int id, string naziv, string opis, string kategorija, string proizvodjac, 
            string dobavljac, decimal cena)
        {

            Proizvodi proizvod = vratiProizvod(id);
            proizvod.Naziv = naziv;
            proizvod.Opis = opis;
            proizvod.Kategorija = kategorija;
            proizvod.Proizvodjac = proizvodjac;
            proizvod.Dobavljac = dobavljac;
            proizvod.Cena = cena;

            try
            {
                ent.SaveChanges();
            }
            catch (Exception ex)
            {
                string por = ex.Message;
            }



        }

        public void unosProizvoda(int id, string naziv, string opis, string kategorija, string proizvodjac,
            string dobavljac, decimal cena)
        {

            Proizvodi proizvod = new Proizvodi();
            proizvod.Id = id;
            proizvod.Naziv = naziv;
            proizvod.Opis = opis;
            proizvod.Kategorija = kategorija;
            proizvod.Proizvodjac = proizvodjac;
            proizvod.Dobavljac = dobavljac;
            proizvod.Cena = cena;
            ent.Proizvodis.Add(proizvod);

            try
            {
                ent.SaveChanges();
            }
            catch (Exception ex)
            {
                string por = ex.Message;
            }



        }

    }
}