﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskProizvodi.Controllers
{
    public class IKontroler
    {
        Kontroler k = new Kontroler();

        public List<Proizvodi> vratiProizvode()
        {
            return k.vratiProizvode();
        }
        public Proizvodi vratiProizvod(int id)
        {
            return k.vratiProizvod(id);
        }
        public void izmenaProizvoda(int id, string naziv, string opis, string kategorija, string proizvodjac,
            string dobavljac, decimal cena)
        {
            k.izmenaProizvoda(id, naziv, opis, kategorija, proizvodjac, dobavljac, cena);
        }
        public void unosProizvoda(int id, string naziv, string opis, string kategorija, string proizvodjac,
            string dobavljac, decimal cena)
        {
            k.unosProizvoda(id, naziv, opis, kategorija, proizvodjac, dobavljac, cena);
        }
    }
}