﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TaskProizvodi.Controllers;

namespace TaskProizvodi
{
    public partial class Proizvodi1 : System.Web.UI.Page
    {
        public IKontroler ki = new IKontroler();

        protected void Page_Load(object sender, EventArgs e)
        {
           
                this.NapuniGrid();
                this.NapuniGridJson();
                this.GridSpojen();
            
        }

        private void NapuniGrid()
        {
            List<Proizvodi> proizvodi = ki.vratiProizvode().ToList();
            DataTable dt = KonvertujListu.ToDataTable<Proizvodi>(proizvodi);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        private void NapuniGridJson()
        {

            string proizvodiJson = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/proizvodiJson.json");
            var proizvodiLista = JsonConvert.DeserializeObject<List<Proizvodi>>(proizvodiJson);
            DataTable dt = KonvertujListu.ToDataTable<Proizvodi>(proizvodiLista);
            GridView2.DataSource = dt;
            GridView2.DataBind();
        }
        private void GridSpojen()
        {
            List<Proizvodi> proizvodi = ki.vratiProizvode().ToList();
            string proizvodiJson = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/proizvodiJson.json");
            var proizvodiLista = JsonConvert.DeserializeObject<List<Proizvodi>>(proizvodiJson);
            DataTable dtBaza = KonvertujListu.ToDataTable<Proizvodi>(proizvodi);
            DataTable dtJson = KonvertujListu.ToDataTable<Proizvodi>(proizvodiLista);

            foreach (DataRow dr in dtJson.Rows)
            {
                dtBaza.Rows.Add(dr.ItemArray);
            }
            GridView3.DataSource = dtBaza;
            GridView3.DataBind();
        }

    }
}