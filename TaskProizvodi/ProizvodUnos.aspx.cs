﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TaskProizvodi.Controllers;

namespace TaskProizvodi
{
    public partial class ProizvodUnos : System.Web.UI.Page
    {
        public IKontroler ki = new IKontroler();

        protected void Page_Load(object sender, EventArgs e)
        {
            List<Proizvodi> proizvodi = ki.vratiProizvode().ToList();
            int max = proizvodi.Max(x => x.Id);
            max = max + 1;
            Id.Text = max.ToString();
            if (!IsPostBack)
            {
                NapuniGrid();
            }

        }

        protected void Unos_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Id.Text.ToString());
            decimal cena = Convert.ToDecimal(Cena.Text.ToString());
            ki.unosProizvoda(id, Naziv.Text, Opis.Text, Kategorija.Text, Proizvodjac.Text, Dobavljac.Text, cena);
            NapuniGrid();
            List<Proizvodi> proizvodi = ki.vratiProizvode().ToList();
            int max = proizvodi.Max(x => x.Id);
            max = max + 1;
            Id.Text = max.ToString();
            Naziv.Text = "";
            Opis.Text = "";
            Kategorija.Text = "";
            Proizvodjac.Text = "";
            Dobavljac.Text = "";
            Cena.Text = "";
        }

        private void NapuniGrid()
        {
            List<Proizvodi> proizvodi = ki.vratiProizvode().ToList();
            DataTable dt = KonvertujListu.ToDataTable<Proizvodi>(proizvodi);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            NapuniGrid();
        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            NapuniGrid();
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString());
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            TextBox GrNaziv = (TextBox)row.Cells[1].Controls[0];
            TextBox GrOpis = (TextBox)row.Cells[2].Controls[0];
            TextBox GrKategorija = (TextBox)row.Cells[3].Controls[0];
            TextBox GrProizvodjac = (TextBox)row.Cells[4].Controls[0];
            TextBox GrDobavljac = (TextBox)row.Cells[5].Controls[0];
            TextBox GrCena = (TextBox)row.Cells[6].Controls[0];
            ki.izmenaProizvoda(id, GrNaziv.Text, GrOpis.Text, GrKategorija.Text, GrProizvodjac.Text, GrDobavljac.Text, Convert.ToDecimal(GrCena.Text));
            NapuniGrid();
        }
    }
}