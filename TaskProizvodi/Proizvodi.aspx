﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Proizvodi.aspx.cs" Inherits="TaskProizvodi.Proizvodi1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Iščitavanje iz baze</h2>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false">
              <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" />
                <asp:BoundField DataField="Naziv" HeaderText="Naziv" />
                <asp:BoundField DataField="Opis" HeaderText="Opis" />
                <asp:BoundField DataField="Kategorija" HeaderText="Kategorija" />
                <asp:BoundField DataField="Proizvodjac" HeaderText="Proizvodjac" />
                <asp:BoundField DataField="Dobavljac" HeaderText="Dobavljac" />
                <asp:BoundField DataField="Cena" HeaderText="Cena" />
              </Columns>
            </asp:GridView>
        </div>
        <br />
        <br />
        <div>
            <h2>Iščitavanje iz Json fajla</h2>
             <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false">
              <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" />
                <asp:BoundField DataField="Naziv" HeaderText="Naziv" />
                <asp:BoundField DataField="Opis" HeaderText="Opis" />
                <asp:BoundField DataField="Kategorija" HeaderText="Kategorija" />
                <asp:BoundField DataField="Proizvodjac" HeaderText="Proizvodjac" />
                <asp:BoundField DataField="Dobavljac" HeaderText="Dobavljac" />
                <asp:BoundField DataField="Cena" HeaderText="Cena" />
              </Columns>
            </asp:GridView>
        </div>
        <br />
        <br />
        <div>
            <h2>Spojeni podaci iz baze i json fajla</h2>
             <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="false">
              <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" />
                <asp:BoundField DataField="Naziv" HeaderText="Naziv" />
                <asp:BoundField DataField="Opis" HeaderText="Opis" />
                <asp:BoundField DataField="Kategorija" HeaderText="Kategorija" />
                <asp:BoundField DataField="Proizvodjac" HeaderText="Proizvodjac" />
                <asp:BoundField DataField="Dobavljac" HeaderText="Dobavljac" />
                <asp:BoundField DataField="Cena" HeaderText="Cena" />
              </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
