﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProizvodUnos.aspx.cs" Inherits="TaskProizvodi.ProizvodUnos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
    <style>
        .label, span {
display: block
}
    </style>
<body>
    <form id="form1" runat="server">

        <div style="width: 100%;">
            <asp:Label id="labelId" Text="Id:" runat="server" />
            <asp:TextBox id="Id" ReadOnly runat="server" />
            <br />
             <br />
            <asp:Label id="labelNaziv" Text="Naziv:" runat="server" />
            <asp:TextBox id="Naziv" runat="server" />
            <br />
             <br />
             <asp:Label id="labelOpis" Text="Opis:" runat="server" />
            <asp:TextBox id="Opis" runat="server" />
            <br />
             <br />
             <asp:Label id="labelKategorija" Text="Kategorija:" runat="server" />
            <asp:TextBox id="Kategorija" runat="server" />
            <br />
             <br />
             <asp:Label id="labelProizvodjac" Text="Proizvodjac:" runat="server" />
            <asp:TextBox id="Proizvodjac" runat="server" />
            <br />
             <br />
             <asp:Label id="labelDobavljac" Text="Dobavljac:" runat="server" />
            <asp:TextBox id="Dobavljac" runat="server" />
            <br />
             <br />
             <asp:Label id="labelCena" Text="Cena:" runat="server" />
            <asp:TextBox id="Cena" runat="server" />
            <br />
             <br />
            <asp:Button id="Unos" Text="Dodaj" OnClick="Unos_Click" runat="server" />

        </div>
            <br />
         <div style="width: 100%;">
            <h2>Proizvodi</h2>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" OnRowEditing="GridView1_RowEditing" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowUpdating="GridView1_RowUpdating">
              <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" />
                <asp:BoundField DataField="Naziv" HeaderText="Naziv" />
                <asp:BoundField DataField="Opis" HeaderText="Opis" />
                <asp:BoundField DataField="Kategorija" HeaderText="Kategorija" />
                <asp:BoundField DataField="Proizvodjac" HeaderText="Proizvodjac" />
                <asp:BoundField DataField="Dobavljac" HeaderText="Dobavljac" />
                <asp:BoundField DataField="Cena" HeaderText="Cena" />
                  <asp:CommandField ShowEditButton="true" />  
              </Columns>
            </asp:GridView>
        </div>
         
    </form>
    
</body>
</html>
